{
  "actors": [
    {
      "id": "509fbe7a-e173-41b2-92fc-bf4b3030eb3c",
      "text": "Avaliador",
      "type": "istar.Role",
      "x": 89,
      "y": 61,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "f108adfd-a8af-44bd-ae82-e35c8c671358",
          "text": "Procurar utilizando multi-filtros por um cuidador",
          "type": "istar.Task",
          "x": 249,
          "y": 145,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "630514d0-2798-4133-8eca-054f4a777aad",
          "text": "Escolher cuidador",
          "type": "istar.Task",
          "x": 259,
          "y": 254,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "3ce08736-8f65-4985-b147-0761b8ee6d86",
          "text": "Resposta rápida do servidor",
          "type": "istar.Quality",
          "x": 89,
          "y": 152,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "4cdf4c2e-746e-4849-8112-c0e6872076fa",
          "text": "Cuidador Avaliado",
          "type": "istar.Goal",
          "x": 264,
          "y": 73,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "353b1a65-847d-4f81-85be-73ad8372732c",
          "text": "Analisar Crítica",
          "type": "istar.Task",
          "x": 258,
          "y": 340,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "9b34e954-6787-46de-8a6c-1c334859f129",
      "text": "Cuidador",
      "type": "istar.Role",
      "x": 288,
      "y": 604,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "43b03076-9af5-4fb8-b9c7-1de70a9ab0e2",
      "text": "Dono de animal",
      "type": "istar.Role",
      "x": 675,
      "y": 330,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "88bdbb9d-585c-4d56-8dd0-896080540e8f",
      "text": "Atribuir selo de qualidade",
      "type": "istar.Task",
      "x": 188,
      "y": 461,
      "customProperties": {
        "Description": ""
      },
      "source": "353b1a65-847d-4f81-85be-73ad8372732c",
      "target": "9b34e954-6787-46de-8a6c-1c334859f129"
    },
    {
      "id": "fd04f35e-255b-4137-a5d0-946ed2b17dde",
      "text": "Banir cuidador",
      "type": "istar.Task",
      "x": 334,
      "y": 462,
      "customProperties": {
        "Description": ""
      },
      "source": "353b1a65-847d-4f81-85be-73ad8372732c",
      "target": "9b34e954-6787-46de-8a6c-1c334859f129"
    },
    {
      "id": "7742acd9-7d3c-4df2-91f6-3713fcc4c289",
      "text": "Crítica",
      "type": "istar.Resource",
      "x": 449,
      "y": 331,
      "customProperties": {
        "Description": ""
      },
      "source": "43b03076-9af5-4fb8-b9c7-1de70a9ab0e2",
      "target": "353b1a65-847d-4f81-85be-73ad8372732c"
    }
  ],
  "links": [
    {
      "id": "e764de34-686f-49fd-9d1c-54705dab3959",
      "type": "istar.ContributionLink",
      "source": "f108adfd-a8af-44bd-ae82-e35c8c671358",
      "target": "3ce08736-8f65-4985-b147-0761b8ee6d86",
      "label": "make"
    },
    {
      "id": "123e3d2e-1569-4a73-824c-8304cc06b373",
      "type": "istar.AndRefinementLink",
      "source": "f108adfd-a8af-44bd-ae82-e35c8c671358",
      "target": "4cdf4c2e-746e-4849-8112-c0e6872076fa"
    },
    {
      "id": "4ae0da01-3d6c-464f-b56b-3ea9bad0dbe2",
      "type": "istar.AndRefinementLink",
      "source": "630514d0-2798-4133-8eca-054f4a777aad",
      "target": "f108adfd-a8af-44bd-ae82-e35c8c671358"
    },
    {
      "id": "2d84fef3-07f8-4fe7-8820-234c1ec0735f",
      "type": "istar.AndRefinementLink",
      "source": "353b1a65-847d-4f81-85be-73ad8372732c",
      "target": "630514d0-2798-4133-8eca-054f4a777aad"
    },
    {
      "id": "069533b1-28e8-4f14-9128-5ce8142a6d8c",
      "type": "istar.DependencyLink",
      "source": "353b1a65-847d-4f81-85be-73ad8372732c",
      "target": "88bdbb9d-585c-4d56-8dd0-896080540e8f"
    },
    {
      "id": "2a5520bd-301a-4228-a756-6ac54a985caf",
      "type": "istar.DependencyLink",
      "source": "88bdbb9d-585c-4d56-8dd0-896080540e8f",
      "target": "9b34e954-6787-46de-8a6c-1c334859f129"
    },
    {
      "id": "fab48066-b6b8-46ee-807a-875f4ab73cb3",
      "type": "istar.DependencyLink",
      "source": "353b1a65-847d-4f81-85be-73ad8372732c",
      "target": "fd04f35e-255b-4137-a5d0-946ed2b17dde"
    },
    {
      "id": "25ceb68b-3cd2-4992-9c6a-4e0d888b62d6",
      "type": "istar.DependencyLink",
      "source": "fd04f35e-255b-4137-a5d0-946ed2b17dde",
      "target": "9b34e954-6787-46de-8a6c-1c334859f129"
    },
    {
      "id": "3784e435-f4cb-42f6-b5e9-d86462f041e3",
      "type": "istar.DependencyLink",
      "source": "43b03076-9af5-4fb8-b9c7-1de70a9ab0e2",
      "target": "7742acd9-7d3c-4df2-91f6-3713fcc4c289"
    },
    {
      "id": "4e90532f-13b2-4e9d-9276-fcfabea141be",
      "type": "istar.DependencyLink",
      "source": "7742acd9-7d3c-4df2-91f6-3713fcc4c289",
      "target": "353b1a65-847d-4f81-85be-73ad8372732c"
    }
  ],
  "display": {
    "f108adfd-a8af-44bd-ae82-e35c8c671358": {
      "width": 115.38748168945312,
      "height": 72.82499694824219
    },
    "3ce08736-8f65-4985-b147-0761b8ee6d86": {
      "width": 106.91940307617188,
      "height": 68.3321762084961
    },
    "353b1a65-847d-4f81-85be-73ad8372732c": {
      "width": 100.6875,
      "height": 41.84375
    },
    "9b34e954-6787-46de-8a6c-1c334859f129": {
      "collapsed": true
    },
    "43b03076-9af5-4fb8-b9c7-1de70a9ab0e2": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Sat, 20 Nov 2021 20:23:22 GMT",
  "diagram": {
    "width": 2000,
    "height": 1447,
    "name": "Avaliador",
    "customProperties": {
      "Description": ""
    }
  }
}