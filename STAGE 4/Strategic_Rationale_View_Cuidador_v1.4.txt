{
  "actors": [
    {
      "id": "6291d91a-a11b-43d2-a3d6-9273f069f3a3",
      "text": "Cuidador",
      "type": "istar.Role",
      "x": 191,
      "y": 291,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
          "text": "Serviços publicitados",
          "type": "istar.Goal",
          "x": 442,
          "y": 383,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d01ea420-83d2-440a-80ef-065de95ea200",
          "text": "Registar conta",
          "type": "istar.Task",
          "x": 428,
          "y": 459,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
          "text": "Sem erros",
          "type": "istar.Quality",
          "x": 568,
          "y": 432,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "61f7b3b0-d28c-482e-a630-cff2bcb73d55",
          "text": "Validar conta",
          "type": "istar.Task",
          "x": 426,
          "y": 556,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "47e92a65-f5dc-4dab-98a7-43de377aaf17",
          "text": "Animal acolhido",
          "type": "istar.Goal",
          "x": 695,
          "y": 533,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d3d860a6-571f-496a-97f9-42c29f04aa85",
          "text": "Receber animal",
          "type": "istar.Task",
          "x": 814,
          "y": 532,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
          "text": "Introduzir NIF",
          "type": "istar.Task",
          "x": 426,
          "y": 642,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "43a12b8c-9fc5-4085-a76d-50d084e02d4f",
          "text": "Serviço pedido",
          "type": "istar.Goal",
          "x": 695,
          "y": 380,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d",
          "text": "Receber pagamento",
          "type": "istar.Task",
          "x": 798,
          "y": 435,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
          "text": "Preencher declaração",
          "type": "istar.Task",
          "x": 571,
          "y": 532,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
          "text": "Animal entregue",
          "type": "istar.Goal",
          "x": 781,
          "y": 629,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8",
          "text": "Aumentar receita",
          "type": "istar.Goal",
          "x": 558,
          "y": 291,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "6422aaa1-581d-42be-b052-780206d80ead",
          "text": "Aumentar Alcance",
          "type": "istar.Quality",
          "x": 265,
          "y": 368,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "bbfb9c75-71d1-408c-bd09-04d3030789d9",
          "text": "Receber Critica Positiva",
          "type": "istar.Task",
          "x": 191,
          "y": 504,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130",
          "text": "Receber Critica Negativa",
          "type": "istar.Task",
          "x": 317,
          "y": 502,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "text": "API Website PetSitting",
      "type": "istar.Agent",
      "x": 439,
      "y": 839,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "9ce67fba-5edd-42aa-b1cc-ea681cf6692d",
      "text": "Cuidador Particular",
      "type": "istar.Role",
      "x": 116,
      "y": 116,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "c1a9482c-0604-4e09-a5e7-419fd00747fe",
      "text": "Empresa",
      "type": "istar.Role",
      "x": 301,
      "y": 102,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "text": "Dono de animal",
      "type": "istar.Role",
      "x": 1227,
      "y": 640,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "text": "Avaliador",
      "type": "istar.Role",
      "x": 122,
      "y": 855,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "e14f284b-be1c-4f27-9b23-282c0ddd2760",
      "text": "Animal",
      "type": "istar.Resource",
      "x": 1099,
      "y": 245,
      "customProperties": {
        "Description": ""
      },
      "source": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11",
      "text": "NIF",
      "type": "istar.Resource",
      "x": 346,
      "y": 716,
      "customProperties": {
        "Description": ""
      },
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c"
    },
    {
      "id": "53fd3212-eb7f-48ac-91f4-016be7af1334",
      "text": "NIF validado",
      "type": "istar.Goal",
      "x": 508,
      "y": 722,
      "customProperties": {
        "Description": ""
      },
      "source": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "target": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf"
    },
    {
      "id": "11f8736a-05a7-4b6f-b5f3-fc4c532bd565",
      "text": "Formulário de registo",
      "type": "istar.Resource",
      "x": 1221,
      "y": 247,
      "customProperties": {
        "Description": ""
      },
      "source": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "62f8042c-e6ac-4744-b5b3-a59942104cb6",
      "text": "Efetuar Transferência",
      "type": "istar.Task",
      "x": 988,
      "y": 246,
      "customProperties": {
        "Description": ""
      },
      "source": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "target": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d"
    },
    {
      "id": "65997198-6448-4859-9c0a-be6eb564aae0",
      "text": "Entregar animal",
      "type": "istar.Task",
      "x": 1002,
      "y": 633,
      "customProperties": {
        "Description": ""
      },
      "source": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
      "target": "f8755a51-5bc3-4827-bf0b-67683cc1fc07"
    },
    {
      "id": "1d8da380-b357-49e3-81ce-98814341cc06",
      "text": "Declaração",
      "type": "istar.Resource",
      "x": 1351,
      "y": 245,
      "customProperties": {
        "Description": ""
      },
      "source": "d3d860a6-571f-496a-97f9-42c29f04aa85",
      "target": "f8755a51-5bc3-4827-bf0b-67683cc1fc07"
    },
    {
      "id": "711c4fb9-a3b7-4e79-a328-4f0c65655bb2",
      "text": "Critica",
      "type": "istar.Resource",
      "x": 96,
      "y": 730,
      "customProperties": {
        "Description": ""
      },
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "bbfb9c75-71d1-408c-bd09-04d3030789d9"
    },
    {
      "id": "383e2470-0483-491a-b50d-494d5a4fbb7c",
      "text": "Critica",
      "type": "istar.Resource",
      "x": 96,
      "y": 730,
      "customProperties": {
        "Description": ""
      },
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130"
    }
  ],
  "links": [
    {
      "id": "adb7a5a1-dc95-45ff-85c7-b8fb3b1c366e",
      "type": "istar.DependencyLink",
      "source": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "target": "e14f284b-be1c-4f27-9b23-282c0ddd2760"
    },
    {
      "id": "f9a4f7e8-2469-4191-92a4-d8b8a9160d1d",
      "type": "istar.DependencyLink",
      "source": "e14f284b-be1c-4f27-9b23-282c0ddd2760",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "39cdaf6c-346b-4d8d-97e5-58843ff57c81",
      "type": "istar.DependencyLink",
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11"
    },
    {
      "id": "ae935425-7a41-487c-9c3a-29621b2148e2",
      "type": "istar.DependencyLink",
      "source": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11",
      "target": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c"
    },
    {
      "id": "e2d34eb2-be7f-4883-bedd-6d3804445bc9",
      "type": "istar.DependencyLink",
      "source": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "target": "53fd3212-eb7f-48ac-91f4-016be7af1334"
    },
    {
      "id": "fd6c0670-1649-42ed-943f-b3efba058d8e",
      "type": "istar.DependencyLink",
      "source": "53fd3212-eb7f-48ac-91f4-016be7af1334",
      "target": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf"
    },
    {
      "id": "70051c20-2e4e-437a-8804-298b5a2fe36e",
      "type": "istar.DependencyLink",
      "source": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "target": "11f8736a-05a7-4b6f-b5f3-fc4c532bd565"
    },
    {
      "id": "61a39821-9eee-437d-a873-676e04866577",
      "type": "istar.DependencyLink",
      "source": "11f8736a-05a7-4b6f-b5f3-fc4c532bd565",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "69481a36-259e-4dcd-a298-03b966b9450d",
      "type": "istar.DependencyLink",
      "source": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "target": "62f8042c-e6ac-4744-b5b3-a59942104cb6"
    },
    {
      "id": "90734c01-8c0a-4973-ad8b-57d077862f11",
      "type": "istar.DependencyLink",
      "source": "62f8042c-e6ac-4744-b5b3-a59942104cb6",
      "target": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d"
    },
    {
      "id": "8496317c-6a63-4166-9d96-8b04aa5b9d80",
      "type": "istar.DependencyLink",
      "source": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
      "target": "65997198-6448-4859-9c0a-be6eb564aae0"
    },
    {
      "id": "a4f8f5c4-4beb-4af8-8374-5f7cabc41f9b",
      "type": "istar.DependencyLink",
      "source": "65997198-6448-4859-9c0a-be6eb564aae0",
      "target": "f8755a51-5bc3-4827-bf0b-67683cc1fc07"
    },
    {
      "id": "91242386-f443-4b04-8fbd-7ca8050de564",
      "type": "istar.DependencyLink",
      "source": "d3d860a6-571f-496a-97f9-42c29f04aa85",
      "target": "1d8da380-b357-49e3-81ce-98814341cc06"
    },
    {
      "id": "93b6b0b0-7ad3-4973-9d62-a4f3aba9fd35",
      "type": "istar.DependencyLink",
      "source": "1d8da380-b357-49e3-81ce-98814341cc06",
      "target": "f8755a51-5bc3-4827-bf0b-67683cc1fc07"
    },
    {
      "id": "3588cbb2-0cb6-43b9-bfd8-f16b270850de",
      "type": "istar.DependencyLink",
      "source": "711c4fb9-a3b7-4e79-a328-4f0c65655bb2",
      "target": "bbfb9c75-71d1-408c-bd09-04d3030789d9"
    },
    {
      "id": "06adfbe0-09c1-4291-817c-1b5e12342337",
      "type": "istar.DependencyLink",
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "711c4fb9-a3b7-4e79-a328-4f0c65655bb2"
    },
    {
      "id": "6a852cfd-085b-4827-856e-746443a55b76",
      "type": "istar.DependencyLink",
      "source": "383e2470-0483-491a-b50d-494d5a4fbb7c",
      "target": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130"
    },
    {
      "id": "1066f7cf-caf6-4007-bb29-1fa5905d5d2b",
      "type": "istar.DependencyLink",
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "383e2470-0483-491a-b50d-494d5a4fbb7c"
    },
    {
      "id": "622389e3-35ff-4c89-8c54-0ea944b3d952",
      "type": "istar.ContributionLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "help"
    },
    {
      "id": "badde61a-5629-43fd-95af-cc85b4e1f5d5",
      "type": "istar.OrRefinementLink",
      "source": "61f7b3b0-d28c-482e-a630-cff2bcb73d55",
      "target": "d01ea420-83d2-440a-80ef-065de95ea200"
    },
    {
      "id": "e860feb7-455c-4681-9cc1-2eeb0350ec8f",
      "type": "istar.AndRefinementLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "ce868adf-9e22-4bac-87f3-dbf8005f91ce"
    },
    {
      "id": "fc112d5b-6c8c-40a5-b509-1550fc78a7b1",
      "type": "istar.IsALink",
      "source": "c1a9482c-0604-4e09-a5e7-419fd00747fe",
      "target": "6291d91a-a11b-43d2-a3d6-9273f069f3a3"
    },
    {
      "id": "0108e325-36b0-45a9-b487-725f9e0d289e",
      "type": "istar.IsALink",
      "source": "9ce67fba-5edd-42aa-b1cc-ea681cf6692d",
      "target": "6291d91a-a11b-43d2-a3d6-9273f069f3a3"
    },
    {
      "id": "d7cd7b2b-5a89-4f99-a3e2-566e68daa04f",
      "type": "istar.AndRefinementLink",
      "source": "d3d860a6-571f-496a-97f9-42c29f04aa85",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "2de80fe7-da70-4ed6-a1b0-85c5b03d056f",
      "type": "istar.AndRefinementLink",
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "61f7b3b0-d28c-482e-a630-cff2bcb73d55"
    },
    {
      "id": "65365ddf-f78b-43da-ab80-cf358f850dd8",
      "type": "istar.AndRefinementLink",
      "source": "47e92a65-f5dc-4dab-98a7-43de377aaf17",
      "target": "43a12b8c-9fc5-4085-a76d-50d084e02d4f"
    },
    {
      "id": "cb8a13c2-7c77-4d96-9410-f9620c09db8d",
      "type": "istar.AndRefinementLink",
      "source": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "1d3050b2-dcb3-4b88-89cf-8b5a32cab9a4",
      "type": "istar.AndRefinementLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "f1477382-f6db-4c82-8d91-d3317616c984",
      "type": "istar.ContributionLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "hurt"
    },
    {
      "id": "7b776e26-e341-43be-a503-769be2f74fe5",
      "type": "istar.AndRefinementLink",
      "source": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
      "target": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8"
    },
    {
      "id": "dc3bd224-dc26-48f1-aaa4-32f715f2eb64",
      "type": "istar.AndRefinementLink",
      "source": "43a12b8c-9fc5-4085-a76d-50d084e02d4f",
      "target": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8"
    },
    {
      "id": "f603badc-4f4a-4301-9d32-749c51f2229d",
      "type": "istar.ContributionLink",
      "source": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "help"
    },
    {
      "id": "817f5e2b-b171-4b10-a48f-c90d7d1c8bfa",
      "type": "istar.ContributionLink",
      "source": "bbfb9c75-71d1-408c-bd09-04d3030789d9",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "help"
    },
    {
      "id": "ef056cb3-96f0-4233-826e-b8b186f09c86",
      "type": "istar.ContributionLink",
      "source": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "hurt"
    }
  ],
  "display": {
    "ad3097c6-2c93-414c-bb09-636d8cfd1d64": {
      "width": 81.80621337890625,
      "height": 46.84869384765625
    },
    "6a852cfd-085b-4827-856e-746443a55b76": {
      "vertices": [
        {
          "x": 164,
          "y": 734
        },
        {
          "x": 276,
          "y": 618
        }
      ]
    },
    "b2daac5a-5316-4de3-a770-f7fc1cac4d2c": {
      "collapsed": true
    },
    "9ce67fba-5edd-42aa-b1cc-ea681cf6692d": {
      "collapsed": true
    },
    "c1a9482c-0604-4e09-a5e7-419fd00747fe": {
      "collapsed": true
    },
    "f8755a51-5bc3-4827-bf0b-67683cc1fc07": {
      "collapsed": true
    },
    "88fa0c2b-36cb-4eaf-95d5-b7a166796a13": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Sat, 20 Nov 2021 18:25:14 GMT",
  "diagram": {
    "width": 2000,
    "height": 1649,
    "name": "Rationale View ( Cuidador )",
    "customProperties": {
      "Description": ""
    }
  }
}
